FROM maven:3.8.1-openjdk-15-slim AS build

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN mvn -f /usr/src/app/pom.xml clean install -DskipTests

FROM openjdk:12-alpine
LABEL maintainer = 'Luiz'

COPY --from=build /usr/src/app/target/*.jar /usr/src/deploy/api.jar
WORKDIR /usr/src/deploy
CMD ["java", "-jar", "-Dspring.profiles.active=production", "api.jar"]

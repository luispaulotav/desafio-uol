# Desafio Uol API

A aplicação **Desafio UOL** é uma aplicação para gerenciamento de produtos.

# Tecnologias

Para criar o sistema foram utilizados as seguintes ferramentas/frameworks:

- Backend com Spring-boot v2.3+ e Java v8 + Banco de Dados H2

# Composição da Stack

A Stack do Desafio UOL API é composta por 1 aplicação (apenas backend), que contém a conexão com o banco de dados (H2), 
e gerenciamento das regras de negócio do sistema.

## O que preciso para subir a aplicação

- Gerenciador de dependencias e build, Maven.

- Java 8.

## Gerando o Pacote
Sendo um projeto Maven, execute os goals clean e install na raiz do projeto para baixar as dependências e gerar jar do projeto

     #!/desafio-uol
     $ mvn clean install
     
## Executando o Jar

Como se trata de um projeto Spring Boot, podemos simplismente executar o jar que foi gerado na pasta target e a 
aplicação irá subir em um tomcat embedded.

    #!/desafio-uol/target
    $ java -jar desafio-uol-0.0.1-SNAPSHOT.jar
    
## Executando os Testes Unitários

    #!/desafio-uol
    $ mvn test    
    

## Executando a documentação Swagger dos endpoints

    http://localhost:9999/desafio-uol/v1/swagger-ui.html   

Configuração da porta da api se encontra no application.yml:
		
	server:
	    port: 9999

Pronto, a aplicação deve estar online na porta 9999.

## Consumir API

Para consumir a API basta acessar por exemplo a url para buscar todos os usuários http://localhost:9999/products.
Existe um script dentro do resources\data.sql para inserção de dados.


### Exemplos:

#### FIND ALL Products:

Entrada:

[ GET ] http://localhost:9999/desafio-uol/v1/products/

Saída: 200 OK

        [
            {
                "id": 1,
                "name": "Sabonete",
                "description": "sabonete dove",
                "price": 2.7
            },
            {
                "id": 2,
                "name": "Café",
                "description": "cafe uniao",
                "price": 7.9
            },
            {
                "id": 3,
                "name": "Biscoito",
                "description": "biscoito mabel",
                "price": 5.7
            },
            {
                "id": 4,
                "name": "Sabão em po",
                "description": "sabao em po omo",
                "price": 10.5
            }
        ]

#### FIND Product BY ID:

Entrada:

[ GET ] http://localhost:9999/desafio-uol/v1/products/1


Saída: 200 OK

        {
            "id": 1,
            "name": "Sabonete",
            "description": "sabonete dove",
            "price": 2.7
        }

#### CREATE Product:

Entrada:

[ POST ] http://localhost:9999/desafio-uol/v1/products

       {
            "name": "Bolacha",
            "description": "Bolacha Mabel 200g",
            "price": 2.99
       }
    
Saída: 200 OK
    
        {
            "id": 5,
            "name": "Bolacha",
            "description": "Bolacha Mabel 200g",
            "price": 2.99
        }

#### UPDATE Product:

Entrada:

[ PUT ] http://localhost:9999/desafio-uol/v1/products/1

       {
            "name": "Sabonete",
            "description": "Novo sabonete dove",
            "price": 3.55
       }

Saída: 200 OK

        {
            "id": 1,
            "name": "Sabonete",
            "description": "Novo sabonete dove",
            "price": 3.55
        }

#### DELETE Product:

Entrada:

[ DELETE ] http://localhost:9999/desafio-uol/v1/products/1

Saída: 200 OK

        {
           "status_code": 200,
           "message": "Produto de id 1 deletado com sucesso."
        }

#### SEARCH Product:

Entrada:

[ GET ] http://localhost:9999/desafio-uol/v1/products/search?min_price=2.90&max_price=3&q=Bolacha

Saída: 200 OK

    [
        {
            "id": 5,
            "name": "Bolacha",
            "description": "Bolacha Mabel 200g",
            "price": 2.99
        }
    ]
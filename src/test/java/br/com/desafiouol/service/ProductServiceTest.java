package br.com.desafiouol.service;


import br.com.desafiouol.dto.ResponseDTO;
import br.com.desafiouol.entity.Product;
import br.com.desafiouol.exception.BadRequestExcetion;
import br.com.desafiouol.repository.ProductRepository;
import br.com.desafiouol.service.implementation.ProductServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductServiceImp productServiceImp;

    @Mock
    private ProductRepository productRepository;

    private Product product;

    @Before
    public void init() {
        product = buildProduct();
    }

    @Test
    public void saveProductTest() {
        Mockito.when(this.productRepository.save(product)).thenReturn(product);
        Product productSaveSucess = new Product();
        productSaveSucess = productServiceImp.saveProduct(product);

        assertEquals(productSaveSucess.getName(), "Sabonete");
        assertEquals(productSaveSucess.getDescription(), "Sabonete Liquido UN");
        assertNotNull(productSaveSucess.getPrice());
    }

    @Test
    public void updateProductTest() {
        Mockito.when(this.productRepository.save(product)).thenReturn(product);
        product.setId(1);
        Product productAtual = new Product();
        productAtual.setName("Sabonete Dove");
        productAtual.setDescription("Sabonete em barra 30un");
        productAtual.setPrice(29.90);

        Mockito.when(this.productRepository.findById(1)).thenReturn(java.util.Optional.of(product));
        Product productAnterior = this.productServiceImp.updateProduct(1, productAtual);
        assertEquals(productAnterior.getName(), "Sabonete Dove");
        assertEquals(productAnterior.getDescription(), "Sabonete em barra 30un");
        assertNotNull(productAnterior.getPrice());
    }

    @Test(expected = BadRequestExcetion.class)
    public void updateProductTestFail() {
        Mockito.when(this.productRepository.save(product)).thenReturn(product);
        Product productAtual = new Product();
        productAtual.setName("Sabonete Dove");
        productAtual.setDescription("Sabonete em barra 30un");
        productAtual.setPrice(29.90);
        this.productServiceImp.updateProduct(product.getId(), productAtual);
    }

    @Test
    public void findProductByIdTest() {
        product.setId(1);
        Mockito.when(this.productRepository.findById(1)).thenReturn(java.util.Optional.of(product));
        Product productSaveSucess = productServiceImp.findProductByIdProduct(1);
        assertEquals(productSaveSucess.getName(), "Sabonete");
        assertEquals(productSaveSucess.getDescription(), "Sabonete Liquido UN");
        assertNotNull(productSaveSucess.getId());

    }

    @Test
    public void deleteProductTest() {
        product.setId(1);
        Mockito.when(this.productRepository.findById(1)).thenReturn(java.util.Optional.of(product));
        ResponseDTO responseDTO =  productServiceImp.deleteProductById(1);
        assertEquals(responseDTO.getMessage() , "Produto de id "+product.getId()+" deletado com sucesso");
    }

    @Test
    public void findProductAllTest() {
        List<Product> products = Arrays.asList(product);
        when(productRepository.findAll()).thenReturn(products);
        List<Product> productList = productServiceImp.findProductAll();

        assertNotNull(productList);
        assertEquals(productList.size(), products.size());
        assertEquals(productList.get(0).getName(), products.get(0).getName());
        assertEquals(productList.get(0).getDescription(), products.get(0).getDescription());
    }

    private Product buildProduct() {
        return Product.builder()
                .name("Sabonete")
                .description("Sabonete Liquido UN")
                .price(9.90)
                .build();
    }
}

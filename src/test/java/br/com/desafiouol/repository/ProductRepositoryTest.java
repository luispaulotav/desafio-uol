package br.com.desafiouol.repository;

import br.com.desafiouol.entity.Product;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    private Product product;

    @Before
    public void init() {
        product = buildProduct();
    }

    @Test
    public void saveProductTest() {
       productRepository.save(product);
        assertEquals(product.getName(), "Sabonete");
        assertEquals(product.getDescription(), "Sabonete Liquido UN");
        assertNotNull(product.getId());
        assertEquals(product.getId(), Integer.valueOf(1));
    }

    @Test
    public void updateProductTest() {
        this.productRepository.save(product);
        Optional<Product> optionalProduct =  this.productRepository.findById(1);
        optionalProduct.get().setDescription("Sabonete Liquido 30 UN");
        optionalProduct.get().setPrice(29.90);

        productRepository.save(optionalProduct.get());
        assertEquals(optionalProduct.get().getName(), "Sabonete");
        assertEquals(optionalProduct.get().getDescription(), "Sabonete Liquido 30 UN");
        assertNotNull(optionalProduct.get().getId());
        assertEquals(optionalProduct.get().getId(), Integer.valueOf(1));

    }

    @Test
    public void findProductByIdTest() {
        productRepository.save(product);
        productRepository.findById(1);
        assertEquals(product.getName(), "Sabonete");
        assertEquals(product.getDescription(), "Sabonete Liquido UN");
        assertNotNull(product.getId());
        assertEquals(product.getId(), Integer.valueOf(1));
    }

    @Test
    public void deleteProductById() {
        productRepository.save(product);
        Optional<Product> optionalProduct = productRepository.findById(1);
        productRepository.delete(optionalProduct.get());
    }

    @Test
    public void findProductAllTest() {
        List<Product> products = Arrays.asList(product);
        productRepository.saveAll(products);
        Iterable<Product> productIterable = productRepository.findAll();
        Assertions.assertThat(productIterable).isNotNull().isNotEmpty();
    }

    private Product buildProduct() {
        return Product.builder()
                .name("Sabonete")
                .description("Sabonete Liquido UN")
                .price(9.90)
                .build();
    }


}

package br.com.desafiouol.resource;

import br.com.desafiouol.entity.Product;
import br.com.desafiouol.service.implementation.ProductServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class ProductResourceTest {

    @InjectMocks
    private ProductResource resource;

    @Mock
    private ProductServiceImp productServiceImp;

    private Product product;

    @Before
    public void init() {
        product = buildProduct();
    }

    @Test
    public void saveProductTest() {
        Product productSaveSucess = product;
        productSaveSucess.setId(1);
        Mockito.when(this.productServiceImp.saveProduct(product)).thenReturn(productSaveSucess);
        ResponseEntity response = resource.saveProduct(product);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateProductTest() {
        Product productAnterior = product;
        Mockito.when(this.productServiceImp.saveProduct(product)).thenReturn(productAnterior);
        productAnterior.setId(1);

        Product productAtual = new Product();
        productAtual.setName("Sabonete Dove");
        productAtual.setDescription("Sabonete em barra 30un");
        productAtual.setPrice(29.90);

        Mockito.when(this.productServiceImp.updateProduct(product.getId(), productAtual)).thenReturn(product);
        buildProductActual(productAnterior, productAtual);

        ResponseEntity response = resource.updateProduct(1, productAtual);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void findProductByIdtest() {
        Product productSaveSucess = product;
        productSaveSucess.setId(1);
        Mockito.when(this.productServiceImp.saveProduct(product)).thenReturn(productSaveSucess);
        Mockito.when(this.productServiceImp.findProductByIdProduct(productSaveSucess.getId())).thenReturn(productSaveSucess);

        ResponseEntity response = resource.findProductById(1);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void deleteProductById() {
        Product productSaveSucess = product;
        productSaveSucess.setId(1);
        Mockito.when(this.productServiceImp.saveProduct(product)).thenReturn(productSaveSucess);
        Mockito.when(this.productServiceImp.findProductByIdProduct(productSaveSucess.getId())).thenReturn(productSaveSucess);

        ResponseEntity response = resource.deleteProductById(1);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void findProductAllTest() {
        List<Product> products = Arrays.asList(product);
        Mockito.when(this.productServiceImp.findProductAll()).thenReturn(products);
        ResponseEntity response = resource.findProductAll();
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }


    private void buildProductActual(Product productAnterior, Product productAtual) {
        productAtual.setName(productAnterior.getName());
        productAtual.setDescription(productAtual.getDescription());
        productAtual.setPrice(productAtual.getPrice());
    }

    private Product buildProduct() {
       return Product.builder()
                .name("Sabonete")
                .description("Sabonete Liquido UN")
                .price(9.90)
                .build();
    }
}

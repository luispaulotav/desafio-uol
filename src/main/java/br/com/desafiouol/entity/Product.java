package br.com.desafiouol.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product extends BaseEntity {

    @NotBlank(message = "name não deve estar em branco.")
    @NotNull(message = "name não deve ser nulo.")
    private String name;

    @NotBlank(message = "description não deve estar em branco.")
    @NotNull(message = "description não deve ser nulo.")
    private String description;

    @Positive(message = "price deve ser valores positivos.")
    @NotNull(message = "price não deve ser nulo.")
    private double price;
}

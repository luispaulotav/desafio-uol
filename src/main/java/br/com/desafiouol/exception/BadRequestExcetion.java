package br.com.desafiouol.exception;

public class BadRequestExcetion extends RuntimeException{

    public BadRequestExcetion(String message) {
        super( message);
    }
}

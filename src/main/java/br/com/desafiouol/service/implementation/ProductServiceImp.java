package br.com.desafiouol.service.implementation;

import br.com.desafiouol.dto.ResponseDTO;
import br.com.desafiouol.entity.Product;
import br.com.desafiouol.exception.BadRequestExcetion;
import br.com.desafiouol.exception.NotFoundException;
import br.com.desafiouol.repository.ProductRepository;
import br.com.desafiouol.service.interfaces.IProductService;
import br.com.desafiouol.specification.ProductSpecification;
import br.com.desafiouol.specification.criteria.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImp implements IProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product saveProduct(Product product) {
        try {
            return productRepository.save(product);
        } catch (BadRequestExcetion e) {
            throw new BadRequestExcetion("erro ao tentar cadastrar a produto. Por favor contate o suporte.");
        }
    }

    @Override
    public Product updateProduct(Integer idProduct, Product productAtual) {
        if (idProduct == null) {
            throw new BadRequestExcetion("Erro ao atualizar produto, idProduct é nulo ou invválido.");
        }
        Optional<Product> optionalProduct = Optional.of(findProductById(idProduct));

        buildProduct(optionalProduct.get(), productAtual);
        try {
            productRepository.save(optionalProduct.get());
        } catch (BadRequestExcetion e) {
            throw new BadRequestExcetion("erro ao tentar atualizar o produto. Por favor contate o suporte.");
        }
        return productAtual;
    }

    @Override
    public Product findProductByIdProduct(Integer idProduct) {
        if (idProduct == null) {
            throw new BadRequestExcetion("Erro ao buscar produto, idProduct é nulo ou invválido.");
        }
        Optional<Product> optionalProduct = Optional.of(findProductById(idProduct));
        return optionalProduct.get();
    }

    @Override
    public ResponseDTO deleteProductById(Integer idProduct) {
        if (idProduct == null) {
            throw new BadRequestExcetion("Erro ao buscar produto, idProduct é nulo ou invválido.");
        }
        Product product = findProductById(idProduct);
        try {
            productRepository.delete(product);
            return ResponseDTO.builder().message("Produto de id "+idProduct+" deletado com sucesso").build();
        } catch (BadRequestExcetion e) {
           throw new BadRequestExcetion("Ocorreu um erro ao tentar deletar o Produto. Por favor contate o suporte. ");
        }
    }

    @Override
    public List<Product> findProductAll() {
        try {
            List<Product> products = (List<Product>) this.productRepository.findAll();
            if (products.isEmpty()) {
               throw new NotFoundException("Não foi possivel encontrar nenhuma lista de produto.");
            }
            return products;
        }catch (BadRequestExcetion e) {
            throw new BadRequestExcetion("Ocorreu um erro ao tentar buscar produtos. Por favor contate o suporte. ");
        }
    }

    @Override
    public List<Product> findProductSearch(Double min_price, Double max_price, String q) {
        if (validParameters(min_price, max_price, q)) {

            ProductSpecification minPriceSpecification = null;
            ProductSpecification maxPriceSpecification = null;
            ProductSpecification nameSpecification = null;
            ProductSpecification descriptionSpecification = null;

            if (min_price != null) {
                minPriceSpecification = getProductSpecificationMaxPrice(min_price);
            }
            if (max_price != null) {
                maxPriceSpecification = getProductSpecificationMinPrice(max_price);
            }
            if (q != null) {
                nameSpecification = getProductSpecificationName(q);
                descriptionSpecification = getProductSpecificationDescription(q);
            }

            Specification<Product> productSpecification = Specification.where(minPriceSpecification)
                    .and(maxPriceSpecification)
                    .and(nameSpecification)
                    .and(descriptionSpecification);

            return productRepository.findAll(productSpecification);
        }
        return new ArrayList<>();
    }

    private ProductSpecification getProductSpecificationDescription(String q) {
        ProductSpecification descriptionSpecification;
        descriptionSpecification = ProductSpecification.builder()
                .criteria(SearchCriteria.builder()
                        .key("description")
                        .value(q)
                        .operation("%")
                        .build())
                .build();
        return descriptionSpecification;
    }

    private ProductSpecification getProductSpecificationName(String q) {
        ProductSpecification nameSpecification;
        nameSpecification = ProductSpecification.builder()
                .criteria(SearchCriteria.builder()
                        .key("name")
                        .value(q)
                        .operation("%")
                        .build())
                .build();
        return nameSpecification;
    }

    private ProductSpecification getProductSpecificationMaxPrice(Double max_price) {
        ProductSpecification maxPriceSpecification;
        maxPriceSpecification = ProductSpecification.builder()
                .criteria(SearchCriteria.builder()
                        .key("price")
                        .value(max_price)
                        .operation(">=")
                        .build())
                .build();
        return maxPriceSpecification;
    }

    private ProductSpecification getProductSpecificationMinPrice(Double min_price) {
        ProductSpecification minPriceSpecification;
        minPriceSpecification = ProductSpecification.builder()
                .criteria(SearchCriteria.builder()
                        .key("price")
                        .value(min_price)
                        .operation("<=")
                        .build())
                .build();
        return minPriceSpecification;
    }

    private boolean validParameters(Double min_price, Double max_price, String q) {
        return !(min_price == null && max_price == null && q == null);
    }

    private void buildProduct(Product productAnterior, Product productAtual) {
        productAnterior.setName(productAtual.getName());
        productAnterior.setDescription(productAtual.getDescription());
        productAnterior.setPrice(productAtual.getPrice());
    }

    private Product findProductById(Integer id) {
        return this.productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Não foi encontrado produto pelo id: "+ id));
    }
}

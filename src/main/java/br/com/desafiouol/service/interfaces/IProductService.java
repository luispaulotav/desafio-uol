package br.com.desafiouol.service.interfaces;

import br.com.desafiouol.dto.ResponseDTO;
import br.com.desafiouol.entity.Product;

import java.util.List;

public interface IProductService {

    Product saveProduct(Product product);

    Product updateProduct(Integer idProduct, Product product);

    Product findProductByIdProduct(Integer idProduct);

    ResponseDTO deleteProductById(Integer idProduct);

    List<Product> findProductAll();

    Iterable<Product> findProductSearch(Double min_price, Double max_price, String q);
}

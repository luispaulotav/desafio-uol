package br.com.desafiouol.resource;

import br.com.desafiouol.dto.ResponseDTO;
import br.com.desafiouol.entity.Product;
import br.com.desafiouol.service.implementation.ProductServiceImp;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/products")
public class ProductResource {

    @Autowired
    private ProductServiceImp productServiceImp;

    @PostMapping(produces = "application/json")
    @ApiOperation(value = "Save Product", response = Product.class)
    public ResponseEntity<?> saveProduct(@Valid @RequestBody Product product) {
        return ResponseEntity.ok(productServiceImp.saveProduct(product));
    }

    @PutMapping(value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Update Product", response = Product.class)
    public ResponseEntity<?> updateProduct(@PathVariable("id") Integer idProduct,
                                           @Valid @RequestBody Product product) {
        return ResponseEntity.ok(productServiceImp.updateProduct(idProduct, product));
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Find Product By ID", response = Product.class)
    public ResponseEntity<?> findProductById(@PathVariable("id") Integer idProduct) {
        return ResponseEntity.ok(productServiceImp.findProductByIdProduct(idProduct));
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Delete Product By ID", response = ResponseDTO.class)
    public ResponseEntity<ResponseDTO> deleteProductById(@PathVariable("id") Integer idProduct) {
        ResponseDTO responseDTO = productServiceImp.deleteProductById(idProduct);
        responseDTO.setStatus_code(HttpStatus.OK.value());
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping(produces = "application/json")
    @ApiOperation(value = "Find All Products", response = Product.class)
    public ResponseEntity<?> findProductAll() {
        return ResponseEntity.ok(productServiceImp.findProductAll());
    }

    @GetMapping(value = "/search", produces = "application/json")
    @ApiOperation(value = "Product Search", response = Product.class)
    public ResponseEntity<?> findProductSearch(@RequestParam(required = false) Double min_price,
                                               @RequestParam(required = false) Double max_price,
                                               @RequestParam(required = false) String q) {
        return ResponseEntity.ok(productServiceImp.findProductSearch(min_price, max_price, q));
    }
}
